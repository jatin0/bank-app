import React, { Component } from "react";
import { Link } from "react-router-dom";
import "../App.css";

class Pagination extends Component {
  state = {
    previousStage: 0,
    nextStage: 10
  };

  previousPage = event => {
    event.preventDefault();
    if (this.state.previousStage > 0 || this.state.nextStage > 10) {
      this.setState({
        previousStage: this.state.previousStage - 1,
        nextStage: this.state.nextStage - 1
      });
    }
  };

  nextPage = event => {
    event.preventDefault();
    this.setState({
      previousStage: this.state.previousStage + 1,
      nextStage: this.state.nextStage + 1
    });
  };

  render() {
    const pageNumbers = [];
    for (
      let i = 1;
      i <= Math.ceil(this.props.totalPage / this.props.detailPerPage);
      i++
    ) {
      pageNumbers.push(i);
    }

    const paging = pageNumbers.slice(
      this.state.previousStage,
      this.state.nextStage
    );

    return (
      <nav aria-label="Page navigation example">
        <ul className="pagination">
          <li className="page-item">
            <div
              onClick={this.previousPage}
              className="page-link"
              // href={this.state.previousStage}
            >
              Previous
            </div>
          </li>

          {paging.map(num => (
            <li key={num} className="page-item">
              {/* <Link to={`http://localhost:3000/${num}`}> */}
              <div
                onClick={() => this.props.changePage(num)}
                className="page-link"
              >
                {num}
              </div>
              {/* </Link> */}
            </li>
          ))}
          <li className="page-item">
            <div
              onClick={this.nextPage}
              className="page-link"
              // href={this.state.nextStage}
            >
              Next
            </div>
          </li>
        </ul>
      </nav>
    );
  }
}

export default Pagination;
