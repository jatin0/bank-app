import React, { Component } from "react";
import Content from "./component/domDetail";
import Pagination from "./component/pagination";
import "./App.css";

class bank extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      isLoading: true,
      SearchInput: "",
      currentPage: 1,
      detailPerPage: 5
    };
  }
  componentDidMount() {
    this.setState({
      isLoading: false
    });
    fetch("https://vast-shore-74260.herokuapp.com/banks?city=MUMBAI")
      .then(res => res.json())
      .then(val =>
        this.setState({
          data: val,
          isLoading: true
        })
      )
      .catch(err => console.log(err));
    // console.log("rendering in componter did mount");
  }

  SearchHandler = event => {
    this.setState({
      SearchInput: event.target.value
    });
  };

  changePage = pagevalue => {
    this.setState({
      currentPage: pagevalue
    });
  };

  render() {
    var { data, SearchInput, currentPage, detailPerPage } = this.state;

    const filterData = data.filter(val =>
      val.branch.toLowerCase().includes(SearchInput.toLowerCase())
    );

    const indexOfLastPage = currentPage * detailPerPage;
    const indexOfFirstPage = indexOfLastPage - detailPerPage;
    const currentDetail = filterData.slice(indexOfFirstPage, indexOfLastPage);

    return (
      <div className="container mt-6">
        <h1 className="mb-3" id="heading">
          BANK APP
        </h1>
        <div className="input-group mb-3">
          <div className="input-group-prepend">
            <span className="input-group-text" id="basic-addon1">
              Search
            </span>
          </div>
          <input
            type="text"
            onChange={this.SearchHandler}
            className="form-control"
            placeholder="Username"
            aria-label="Username"
            aria-describedby="basic-addon1"
          />
        </div>
        {this.state.isLoading ? (
          <React.Fragment>
            <Content data={currentDetail} />
            <Pagination
              detailPerPage={detailPerPage}
              totalPage={data.length}
              changePage={this.changePage}
            />
          </React.Fragment>
        ) : (
          <React.Fragment>
            <div className="spinner-grow text-primary" role="status">
              <span className="sr-only">Loading...</span>
            </div>
            <div className="spinner-grow text-secondary" role="status">
              <span className="sr-only">Loading...</span>
            </div>
            <div className="spinner-grow text-success" role="status">
              <span className="sr-only">Loading...</span>
            </div>
            <div className="spinner-grow text-danger" role="status">
              <span className="sr-only">Loading...</span>
            </div>
            <div className="spinner-grow text-warning" role="status">
              <span className="sr-only">Loading...</span>
            </div>
            <div className="spinner-grow text-info" role="status">
              <span className="sr-only">Loading...</span>
            </div>
            <div className="spinner-grow text-light" role="status">
              <span className="sr-only">Loading...</span>
            </div>
            <div className="spinner-grow text-dark" role="status">
              <span className="sr-only">Loading...</span>
            </div>
          </React.Fragment>
        )}
      </div>
    );
  }
}

export default bank;
