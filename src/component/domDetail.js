import React, { Component } from "react";

class Details extends Component {
  state = {};
  render() {
    return (
      <div>
        <table className="table">
          <thead className="thead-dark">
            <tr>
              <th scope="col">IFSC Code</th>
              <th scope="col">Bank Id</th>
              <th scope="col">Branch</th>
              <th scope="col">Address</th>
              <th scope="col">City</th>
              <th scope="col">District</th>
              <th scope="col">State</th>
              <th scope="col">Bank Name</th>
            </tr>
          </thead>
          {this.props.data.map(val => {
            return (
              <tbody>
                <tr>
                  <td>{val.ifsc}</td>
                  <td>{val.bank_id}</td>
                  <td>{val.branch}</td>
                  <td>{val.address}</td>
                  <td>{val.city}</td>
                  <td>{val.district}</td>
                  <td>{val.state}</td>
                  <td>{val.bank_name}</td>
                </tr>
              </tbody>
            );
          })}
        </table>
      </div>
    );
  }
}

export default Details;
